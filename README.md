# kubectl

Install and manage kubectl

## Dependencies

* [polka.asdf](https://gitlab.com/discr33t/polka/asdf.git)
  _The asdf configs must be included in the users `playbook.yml` since no
  default configs are passed to the dependent role_

## Role Variables

* `versions`:
    * Type: List
    * Usages: List of kubectl versions to install

* `global_version`:
    * Type: String
    * Usages: Kubectl version to make global default

```
kubectl:
  versions:
    - 1.8.3
    - 1.10.7
  global_default: 1.8.3
```

## Example Playbook

Including an example of how to use your role (for instance, with variables
passed in as parameters) is always nice for users too:

    - hosts: servers
      roles:
         - polka.kubectl

## License

MIT
